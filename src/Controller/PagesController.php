<?php

namespace App\Controller;

use App\Entity\Painting;
use App\Form\ArtFormType;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('pages/index.html.twig', [
            'controller_name' => 'PagesController',
        ]);
    }

    #[Route('/gallery', name: 'gallery')]
    public function gallery(): Response
    {
        $paintings = $this->getDoctrine()
                            ->getRepository(Painting::class)
                            ->findAll();
        return $this->render('pages/gallery.html.twig', [
            'controller_name' => 'PagesController',
            'paintings' => $paintings,
        ]);
    }

    #[Route('/detail/{id}', name: 'detail')]
    /**
     * @param Painting $paint
     * @return Response
     */

    public function detail(Painting $paint): Response
    {
        return $this->render('pages/detail.html.twig', [
            'paint' => $paint
        ]);
    }

    #[Route('/about', name: 'about')]
    public function about(): Response
    {
        return $this->render('pages/about.html.twig', [
            'controller_name' => 'PagesController',
        ]);
    }

    #[Route('/team', name: 'team')]
    public function team(): Response
    {
        return $this->render('pages/team.html.twig', [
            'controller_name' => 'PagesController',
        ]);
    }

    #[Route('/admin', name: 'admin')]
    public function admin(AuthorRepository $repository, Request $request): Response
    {
        $paintings = $this->getDoctrine()
            ->getRepository(Painting::class)
            ->findAll();
        $authors = $repository->findBy(
            [],
            ['name' => "ASC"]
        );

        return $this->render('pages/admin.html.twig', [
            'controller_name'   => 'PagesController',
            'paintings'         => $paintings,
            'authors'           => $authors,
        ]);
    }

    #[Route('/newart', name: 'newart')]
    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */

    public function newArt(Request $request, EntityManagerInterface $manager): Response
    {
        $art = new Painting;
        $form = $this->createForm(ArtFormType::class, $art);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $art->setImage('default.jpeg');
            $art->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($art);
            $manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('pages/newart.html.twig', ['form' => $form]);
    }

    #[Route('/editart/{id}', name: 'editart')]
    /**
     * @param Painting $paint
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */

    public function editArt(Painting $paint, EntityManagerInterface $manager, Request $request)
    {
        $form = $this->createForm(ArtFormType::class, $paint);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $paint->setImage('default.jpeg');
            $paint->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($paint);
            $manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('pages/editart.html.twig', ['form' => $form]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    /**
     * @param Painting $paint
     * @param EntityManagerInterface $manager
     * @return Response
     */

    public function delete(Painting $paint, EntityManagerInterface $manager): Response
    {
        $manager->remove($paint);
        $manager->flush();
        return $this->redirectToRoute('admin');
    }


}
