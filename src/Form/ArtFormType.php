<?php

namespace App\Form;

use App\Entity\Painting;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label'         => 'Titre de l\'oeuvre'
            ])
            ->add('description', TextareaType::class, [
                'label'         => 'Description de l\'oeuvre'
            ])
            ->add('height', IntegerType::class, [
                'label'         => 'Hauteur de l\'oeuvre en pixel (px)'
            ])
            ->add('width', IntegerType::class, [
                'label'         => 'Largeur de l\'oeuvre en pixel (px)'
            ])
            ->add('author', EntityType::class, [
                'label'         => 'Sélectionnez l\'auteur de l\'oeuvre',
                'placeholder'   => 'Sélectionnez...',
                'class'         => 'App:Author',
                'choice_label'  => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Painting::class,
        ]);
    }
}
